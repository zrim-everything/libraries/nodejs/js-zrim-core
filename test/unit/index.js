const TestLauncher = require('js-zrim-test-bootstrap').TestLauncher,
  TestLauncherConfigBuilder = require('js-zrim-test-bootstrap').TestLauncherConfigBuilder;

const testLauncher = new TestLauncher(),
  configBuilder = new TestLauncherConfigBuilder();

configBuilder
  .projectConfiguration()
  .rootDirectoryPath(__dirname + '/../..')
  .parentBuilder()
  .testConfiguration()
  .unitTest();

testLauncher.configure(configBuilder.build())
  .then(() => testLauncher.run())
  .catch(error => {
    process.stderr.write(`Something goes wrong: ${error.message}\n${error.stack}`);
  });
